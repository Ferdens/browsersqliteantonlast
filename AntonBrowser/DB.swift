import Foundation
import SQLite

class DB{
    
    var queryStatement   : OpaquePointer? = nil
    var DB               : OpaquePointer?
    let selectError      = "SELECT statement could not be prepared"
    let connectionSucces = "Successfully opened connection to database."
    let connectionUnable = "Unable to open database. Verify that your path is true"
    
    //MARK: Data base opening
    func openDatabase(pathToDB: String) -> (String,Bool) {
        var isConnected = false
        var result : String?
        if sqlite3_open(pathToDB, &self.DB) == SQLITE_OK {
            result = connectionSucces
            isConnected = true
        }
        else {
            result = connectionUnable
            isConnected = false
        }
        
        return (result!,isConnected)
    }
    //MARK: Insert,Update,Delete,Create Table
    func universalQuery(query: String){
        if query != "" {
            let queryStatementString = "\(query)"
            sqlite3_exec(self.DB!, queryStatementString, nil, nil, nil)
        }
    }
    //MARK: Table creation
    func createTable(tableQuery: String){
        if tableQuery != "" {
            let queryStatementStringCreate = "\(tableQuery)"
            sqlite3_exec(self.DB!, queryStatementStringCreate, nil, nil, nil)
        }
        
    }
    
    //MARK: Insert method
    func insert(insertQuery: String){
        let queryStatementStringInsert = "\(insertQuery)"
        sqlite3_exec(DB, queryStatementStringInsert, nil, nil, nil)
    }
    
    //MARK: Select method
    func selectAll(sqlQuery: String) -> [String]{
        var result = [String]()
        let queryStatementString = "\(sqlQuery)"
        let sql3 = sqlite3_prepare_v2(self.DB, queryStatementString, -1, &queryStatement, nil)
        var tempRes = ""
        let countOfColumns = sqlite3_column_count(queryStatement)
        if sql3 == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                tempRes = ""
                for i in 0...countOfColumns {
                    if let val = sqlite3_column_text(queryStatement,i){
                        let valRes = String(cString: val)
                        tempRes += valRes + "\t"
                    }
                }
                result.append(tempRes)
            }
            
        } else {
            result.append(selectError)
        }
        sqlite3_finalize(queryStatement)
        return result
    }
}

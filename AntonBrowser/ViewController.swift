//
//  ViewController.swift
//  AntonBrowser
//
//  Created by Anton on 27.11.16.
//  Copyright © 2016 Macbook. All rights reserved.
//

import Cocoa
import SQLite

class ViewController: NSViewController {
    
    var count = 0
    var dataBase = DB()
    
    @IBOutlet weak var dataBasePath             : NSTextField!
    @IBOutlet weak var isConnected              : NSTextField!
    @IBOutlet weak var universalQueryText       : NSTextField!
    @IBOutlet weak var universalButtonOutlet    : NSButton!
    @IBOutlet weak var clearButtonOutlet        : NSButton!
    @IBOutlet weak var selectQueryButtonOutlet  : NSButton!
    @IBOutlet weak var selectQueryText          : NSTextField!
    @IBOutlet weak var selectResultTextField    : NSTextField!
    
    //MARK: SelectButton
    @IBAction func selectButton(_ sender: NSButton) {
        self.selectResultTextField.stringValue = ""
        for i in dataBase.selectAll(sqlQuery: self.selectQueryText.stringValue){
            self.selectResultTextField.stringValue += i + "\n"
        }
    }
    //MARK: Insert,Update,Delete,Create Table
    @IBAction func universalQuery(_ sender: NSButton) {
        dataBase.universalQuery(query: self.universalQueryText.stringValue)
    }
    
    //MARK: Data base connection
    @IBAction func connect(_ sender: NSButton) {
        
        let resultOfConnection = dataBase.openDatabase(pathToDB: self.dataBasePath.stringValue)
        self.isConnected.stringValue = resultOfConnection.0
        self.outletsEnable(enable: resultOfConnection.1)
    }
    @IBAction func clearData(_ sender: NSButton) {
        self.selectQueryText.stringValue = ""
    }
    
    //MARK: Outlets enable method
    func outletsEnable(enable: Bool){
        self.universalQueryText.isEnabled      = enable
        self.universalButtonOutlet.isEnabled   = enable
        self.selectQueryButtonOutlet.isEnabled = enable
        self.clearButtonOutlet.isEnabled       = enable
        self.selectQueryText.isEnabled         = enable
        self.selectResultTextField.isEnabled   = enable
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.outletsEnable(enable: false)
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    
}

